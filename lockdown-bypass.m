// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <CydiaSubstrate/CydiaSubstrate.h>
#include <Foundation/Foundation.h>

#include <os/log.h>
#include <objc/objc.h>

void (*origEvalPolicy)(
    id self, SEL _cmd,
    NSInteger policy,
    id options,
    void (^reply)(NSDictionary*, NSError*)
);

void myEvalPolicy(
    id self, SEL _cmd,
    NSInteger policy,
    id options,
    void (^reply)(NSDictionary* dict, NSError* error)
) {
    os_log(OS_LOG_DEFAULT, "MS: lockdown called LAContext evaluatePolicy:%ld options:%@ reply:%@", (long)policy, options, reply);
#if 0
    origEvalPolicy(self, _cmd, policy, options, ^(NSDictionary* dict, NSError* error) {
        os_log(OS_LOG_DEFAULT, "MS: evaluatePolicy called block with dict:%@ error:%@", dict, error);
        reply(dict, error);
    });
#else
    os_log(OS_LOG_DEFAULT, "MS: Bypassing passcode prompt to authorize accessory\n");
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        os_log(OS_LOG_DEFAULT, "MS: Calling localauth reply block");
        reply(@{@3: @1}, nil);
    });
#endif
}

static CFUserNotificationRef trustPromptNotif = NULL;

CFUserNotificationRef (*origCFUserNotificationCreate)(CFAllocatorRef allocator, CFTimeInterval timeout, CFOptionFlags flags, SInt32 *error, CFDictionaryRef dictionary);
SInt32 (*origCFUserNotificationReceiveResponse)(CFUserNotificationRef userNotification, CFTimeInterval timeout, CFOptionFlags *responseFlags);

static CFUserNotificationRef myCFUserNotificationCreate(CFAllocatorRef allocator, CFTimeInterval timeout, CFOptionFlags flags, SInt32 *error, CFDictionaryRef dictionary)
{
    os_log(OS_LOG_DEFAULT, "MS: called CFUserNotificationCreate with dict %@", dictionary);
    // TODO look into the dictionary to see if this is actually
    // the prompt we're looking for
    trustPromptNotif = origCFUserNotificationCreate(allocator, timeout, flags, error, dictionary);
    os_log(OS_LOG_DEFAULT, "MS: created notif %p (%@)", trustPromptNotif, trustPromptNotif);
    return trustPromptNotif;
}
static SInt32 myCFUserNotificationReceiveResponse(CFUserNotificationRef userNotification, CFTimeInterval timeout, CFOptionFlags *responseFlags) {
    if (userNotification == trustPromptNotif) {
        os_log(OS_LOG_DEFAULT, "MS: called CFUserNotificationReceiveResponse with notif %p (%@)", userNotification, userNotification);
#if 0
        os_log(OS_LOG_DEFAULT, "MS: waiting for response...");
        SInt32 retval = origCFUserNotificationReceiveResponse(userNotification, timeout, responseFlags);
        os_log(OS_LOG_DEFAULT, "MS: retval %d, flags %lu", (int)retval, *responseFlags);
        return retval;
#else
        sleep(2);
        os_log(OS_LOG_DEFAULT, "MS: will cancel prompt");
        CFUserNotificationCancel(userNotification);
        os_log(OS_LOG_DEFAULT, "MS: returning ok");
        *responseFlags = kCFUserNotificationAlternateResponse;
        return 0;
#endif
    } else {
        // call original
        return origCFUserNotificationReceiveResponse(userNotification, timeout, responseFlags);
    }
}

MSInitialize {
    os_log(OS_LOG_DEFAULT, "SBUnlock-lockdown: loading");

    Class laClass = objc_getClass("LAContext");
    if (laClass) {
        os_log(OS_LOG_DEFAULT, "MS: LAContext class found, hooking");
        MSHookMessageEx(
            laClass,
            @selector(evaluatePolicy:options:reply:),
            (IMP)&myEvalPolicy,
            (IMP*)&origEvalPolicy
        );
    }
    MSHookFunction(&CFUserNotificationCreate, myCFUserNotificationCreate, (void*)&origCFUserNotificationCreate);
    MSHookFunction(&CFUserNotificationReceiveResponse, myCFUserNotificationReceiveResponse, (void*)&origCFUserNotificationReceiveResponse);

}
