// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <CydiaSubstrate/CydiaSubstrate.h>
#include <Foundation/Foundation.h>

#include <os/log.h>
#include <objc/objc.h>
#include "xpc/xpc.h"

#include "rocketbootstrap/rocketbootstrap.h"

@interface SBLockScreenManager : NSObject
+(SBLockScreenManager*) sharedInstance;
-(id)attemptUnlockWithPasscode:(NSString*)code;
@end

void startService() {
    xpc_connection_t listener = rocketbootstrap_xpc_connection_create(
        "xyz.nicolas17.sbunlock",
        NULL,
        XPC_CONNECTION_MACH_SERVICE_LISTENER
    );

    if (!listener) {
        os_log(OS_LOG_DEFAULT, "failed to start service");
        return;
    }
    os_log(OS_LOG_DEFAULT, "service started");
    xpc_connection_set_event_handler(listener, ^(xpc_object_t peer) {
        os_log(OS_LOG_DEFAULT, "client connected");
        xpc_connection_set_target_queue(peer, dispatch_get_main_queue());
        xpc_connection_set_event_handler(peer, ^(xpc_object_t object) {
            xpc_type_t type = xpc_get_type(object);
            if (type == XPC_TYPE_DICTIONARY) {
                char *desc = xpc_copy_description(object);
                os_log(OS_LOG_DEFAULT, "client sent a message: %s", desc);
                free(desc);
                int64_t commandId = xpc_dictionary_get_int64(object, "command");
                if (commandId == 1) {
                    const char* code = xpc_dictionary_get_string(object, "passcode");
                    if (code) {
                        [[objc_getClass("SBLockScreenManager") sharedInstance] attemptUnlockWithPasscode:[NSString stringWithUTF8String:code]];
                    }
                    xpc_object_t reply = xpc_dictionary_create_reply(object);
                    if (reply) {
                        xpc_dictionary_set_string(reply, "result", "success");
                        xpc_connection_send_message(peer, reply);
                    } else {
                        os_log(OS_LOG_DEFAULT, "xpc_dictionary_create_reply failed");
                    }
                } else {
                    xpc_object_t reply = xpc_dictionary_create_reply(object);
                    if (reply) {
                        xpc_dictionary_set_string(reply, "result", "unknown command");
                        xpc_connection_send_message(peer, reply);
                    } else {
                        os_log(OS_LOG_DEFAULT, "xpc_dictionary_create_reply failed");
                    }
                }
            } else if (type == XPC_TYPE_ERROR) {
                os_log(OS_LOG_DEFAULT, "client disconnected");
            } else {
                os_log(OS_LOG_DEFAULT, "received unexpected XPC object");
            }
        });
        xpc_connection_resume(peer);
    });
    xpc_connection_resume(listener);
}
void (*origFinishLaunching)(
    id self, SEL _cmd,
    int app
);
void myFinishLaunching(
    id self, SEL _cmd,
    int app
) {
    origFinishLaunching(self, _cmd, app);

    startService();
}
MSInitialize {
    os_log(OS_LOG_DEFAULT, "SBUnlock: loading");

    Class sbClass = objc_getClass("SpringBoard");
    if (sbClass) {
        os_log(OS_LOG_DEFAULT, "Tweak: SpringBoard class found, hooking");
        MSHookMessageEx(
            sbClass,
            @selector(applicationDidFinishLaunching:),
            (IMP)&myFinishLaunching,
            (IMP*)&origFinishLaunching
        );
    }
}
