// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdlib.h>

#include <dispatch/dispatch.h>
#include "xpc/xpc.h"
#include "rocketbootstrap/rocketbootstrap.h"

int main(int argc, const char** argv)
{
    if (argc != 2) {
        printf("Usage: %s <passcode>\n", argv[0]?argv[0]:"unlock-screen");
        return 1;
    }
    const char* passcode = argv[1];

    xpc_connection_t conn = rocketbootstrap_xpc_connection_create("xyz.nicolas17.sbunlock", NULL, 0);

    if (!conn) {
        fprintf(stderr, "Failed to connect?!\n");
        return 1;
    }

    xpc_connection_set_event_handler(conn, ^(xpc_object_t obj) {
        if (xpc_get_type(obj) == XPC_TYPE_DICTIONARY) {
            int64_t commandId = xpc_dictionary_get_int64(obj, "command");
            if (commandId == 4) {
                const char* data = xpc_dictionary_get_string(obj, "data");
                printf("%s\n", data);
                fflush(stdout);
            } else {
                fprintf(stderr, "Got unknown XPC message #%lld\n", commandId);
            }
        } else if (xpc_get_type(obj) == XPC_TYPE_ERROR) {
            fprintf(stderr, "Got an error: %s\n", xpc_dictionary_get_string(obj, XPC_ERROR_KEY_DESCRIPTION));
            exit(1);
        }
    });

    xpc_connection_resume(conn);

    dispatch_async(dispatch_get_main_queue(), ^() {
        const char* keys[] = {"command", "passcode"};
        xpc_object_t values[] = {xpc_int64_create(1), xpc_string_create(passcode)};
        xpc_object_t msg = xpc_dictionary_create(keys, values, 2);
        xpc_connection_send_message_with_reply(conn, msg, dispatch_get_main_queue(), ^(xpc_object_t obj) {
            if (xpc_get_type(obj) == XPC_TYPE_DICTIONARY) {
                const char* data = xpc_dictionary_get_string(obj, "result");
                if (data) {
                    printf("reply handler got %s\n", data);
                    fflush(stdout);
                    exit(0);
                } else {
                    fprintf(stderr, "reply handler got unknown XPC message\n");
                }
            } else if (xpc_get_type(obj) == XPC_TYPE_ERROR) {
                fprintf(stderr, "reply handler got an error: %s\n", xpc_dictionary_get_string(obj, XPC_ERROR_KEY_DESCRIPTION));
                exit(1);
            }
        });
        fprintf(stderr, "sent data\n");
        //exit(0);
    });

    dispatch_main();
}
