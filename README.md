# SBUnlock

This is a MobileSubstrate tweak that
unlocks an iOS device given the passcode.
Useful to get it unlocked when the screen is broken.
